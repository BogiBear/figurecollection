//
//  ContentView.swift
//  FigureCollection
//
//  Created by Tudosie, Bogdan on 11.10.2021.
//

import SwiftUI
import CoreData
import Combine

// expand to add detailview
struct ContentView: View {
    @State var showingPopover = false
    @Environment(\.managedObjectContext) private var viewContext

    // Fetch Request for Collector items
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Figure.name, ascending: true)], animation: .default)
    
    private var collectedItems: FetchedResults<Figure>
    
    var body: some View {
        NavigationView {
            List {
                ForEach(collectedItems) {
                    figure in NavigationLink(destination: FigureDetailView(figureName: figure.name!,
                                                                           figureDescription: figure.figureDescription!,
                                                                           isPainted: figure.painted,
                                                                           dateAcquired: convertDateToString(date: figure.dateAcquired!),
                                                                           photoUrl: figure.photoUrl!)) {
                        Text(figure.name!)
                    }
                }
                .onDelete(perform: deleteItems)
            }
            .toolbar {
                ToolbarItem {
                    Button(action:  {self.showingPopover.toggle()}) {
                        Label("Add Item", systemImage: "plus")
                    }.popover(isPresented: self.$showingPopover, arrowEdge: .bottom) {
                        AddFigurePopover(figureName: "",
                                         figureDescription: "",
                                         isPainted: false,
                                         dateAcquired: Date(),
                                         photoUrl: "")
                    }.buttonStyle(PlainButtonStyle())
                }
            }
            Text("Select an item")
        }
    }
    
    private func convertDateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        return dateFormatter.string(from: date)
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { collectedItems[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

// TODO: Complete FigureDetailView
struct FigureDetailView: View {
    @State var figureName: String
    @State var figureDescription: String
    @State var isPainted: Bool
    @State var dateAcquired: String
    @State var photoUrl: String // TODO: convert this to UIImage later
    
    @ObservedObject var imageLoader = ImageLoader()
    
    var body: some View{
        VStack(alignment: .leading){
            HStack{
                Text("Figure name")
                TextField("Enter figure name", text: $figureName)
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
           HStack{
               Text("Description")
               TextField("Enter figure name", text: $figureDescription)
           }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
           HStack{
               Toggle("Is Painted", isOn: $isPainted)
           }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
           HStack {
               Text("Date Acquired")
               TextField("", text: $dateAcquired)
           }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
           HStack(alignment: .center) {
               if self.imageLoader.image != nil {
                   Image(nsImage: self.imageLoader.image!).aspectRatio(contentMode: .fit).shadow(radius: 1).cornerRadius(5)
               }
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
        }
        .onAppear {
            let url =  URL(string: photoUrl)
            self.imageLoader.loadImage(with: url!)
        }
    }
}

struct AddFigurePopover: View {
    
    @EnvironmentObject var figureToAdd: Figure
    @Environment(\.managedObjectContext) private var viewContext
    
    @State var figureName: String
    @State var figureDescription: String
    @State var isPainted: Bool
    @State var dateAcquired: Date
    @State var photoUrl: String
    var body: some View {
        VStack {
            Text("Add a figure to your collection").padding(EdgeInsets(top: 10, leading: 5, bottom: 5, trailing: 5))
            HStack {
                Text("Figure name")
                TextField("Enter figure name", text: $figureName)
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
            HStack {
                Text("Figure description")
                TextField("Enter Description", text: $figureDescription)
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
            HStack {
                Toggle("Is Painted", isOn: $isPainted)
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
            HStack {
                DatePicker("Date Acquired", selection: $dateAcquired)
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
            HStack {
                Text("Photo URL")
                TextField("Enter Photo resource URL", text: $photoUrl)
            }.padding(EdgeInsets(top: 2.5, leading: 5, bottom: 2.5, trailing: 5))
            Button("Add to collection") {
                addItem()
            }.padding(EdgeInsets(top: 5, leading: 5, bottom: 10, trailing: 5))
        }
    }
    
    private func addItem() {
        let newFigure = Figure(entity: Figure.entity(), insertInto: viewContext)
        newFigure.name = figureName
        newFigure.figureDescription = figureDescription
        newFigure.painted = isPainted
        newFigure.dateAcquired = dateAcquired
        newFigure.photoUrl = photoUrl
        
        do {
            try viewContext.save()
        }
        catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
