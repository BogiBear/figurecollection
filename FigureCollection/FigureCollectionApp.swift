//
//  FigureCollectionApp.swift
//  FigureCollection
//
//  Created by Tudosie, Bogdan on 11.10.2021.
//

import SwiftUI

@main
struct FigureCollectionApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
