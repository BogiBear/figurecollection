//
//  Figure+CoreDataClass.swift
//  FigureCollection
//
//  Created by Tudosie, Bogdan on 11.10.2021.
//
//

import Foundation
import CoreData
import SwiftUI

@objc(Figure)
public class Figure: NSManagedObject {
    @State var figure: Figure?
}
