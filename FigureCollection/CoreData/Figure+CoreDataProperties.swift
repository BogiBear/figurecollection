//
//  Figure+CoreDataProperties.swift
//  FigureCollection
//
//  Created by Tudosie, Bogdan on 11.10.2021.
//
//

import Foundation
import CoreData


extension Figure {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Figure> {
        return NSFetchRequest<Figure>(entityName: "Figure")
    }
    

    @NSManaged public var name: String?
    @NSManaged public var figureDescription: String?
    @NSManaged public var painted: Bool
    @NSManaged public var dateAcquired: Date?
    @NSManaged public var photoUrl: String?

}

extension Figure : Identifiable {

}
